/*global FB $*/

window.fbAsyncInit = function() {
	FB.init({
		appId: "426267801104829",
		autoLogAppEvents: true,
		xfbml: true,
		status: true,
		oauth: true,
		version: 'v2.10'
	});
				
	FB.AppEvents.logPageView();

/*
	FB.getLoginStatus(function(response) {
	  	if (response.status === 'connected') {
	    	var uid = response.authResponse.userID;
	    	var accessToken = response.authResponse.accessToken;
	    	apiCall();
	  	}
	  	else if (response.status === 'not_authorized') {
	  	  	console.log("not auth");
	  	} 
	  	else {
		  	FB.login(function(response) {
		  		if (response.status === 'connected') {
		  	    	var uid = response.authResponse.userID;
		  	    	var accessToken = response.authResponse.accessToken;
		  	    	apiCall();
		  		}
		  	});
	  	} 
	});*/
	apiCall();
};

(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "http://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function apiCall() {
	let gallery_id = qs.get("id");
  	let apiCall;
  	if(gallery_id) {
  		apiCall = "/" + gallery_id + "/photos?fields=link,images,name&" + window.x();
  	}
  	else {
  		apiCall = "/TheNLemon/albums?fields=name,photos.limit(1){images}&" + window.x();
  	}
	FB.api(apiCall, function (response) {
	    if (response && !response.error) {
	        if(gallery_id) {
	        	gallery_init(response);
	        }
	        else {
	        	gallery_page_init(response);
	        }
	    }
	  	else {
	  		console.log("error loading album...")
	  	}
	});
}

function gallery_init(photos) {
	let gallery_length = photos.data.length;
	let $gallery_title = $("<h1 />", {"id":"gallery_title"});
	let $title_image = $("<img />", {"id":"title_image", "src":"https://static1.squarespace.com/static/590aca259de4bbeaf8492613/t/5914b0cd2994ca1003a8d1c5/1502427178758/?format=1500w"})
	let $title_div = $("<div />", {"id":"title_div", "class":"title_div"});
	let $gallery_div = $("<div />", {"id":"gallery_div", "class":"mason_gallery"});
	let $curr_pic;
	let $curr_link;

	//console.log(photos);

	$($gallery_title).text("The Naked Lemon");
	//$($title_div).append($gallery_title);
	$($title_div).append($title_image);
	//$("body").append($title_div);
	$("#root").after($gallery_div);

	for(let i = 0; i < gallery_length; i++) {
		$curr_pic = $("<img />", {"class":"mason_img", "src":photos.data[i].images[0].source});
		$curr_link = $("<a />", {"target":"_blank", "href":photos.data[i].link});
		$curr_link.append($curr_pic);
		$($gallery_div).append($curr_link);
	}
}


function gallery_page_init(album_data) {
	let $album_title = $("<h1 />", {"id":"album_title", "class":"album_title"});
	let $title_image = $("<img />", {"id":"title_image", "src":"https://static1.squarespace.com/static/590aca259de4bbeaf8492613/t/5914b0cd2994ca1003a8d1c5/1502427178758/?format=1500w"});
	let $title_div = $("<div />", {"id":"title_div", "class":"title_div"});
	let $gallery_div = $("<div />", {"id":"gallery_selection_div", "class":"gallery_selection_div"});
	let new_gallery_page = album_data;
	let $curr_pic;
	let $img_container;
	let $middle;
	let $gallery_link;

	$($title_div).append($title_image);
	//$("body").append($title_div);
	$("#root").after($gallery_div);

	for(let i = 0; i < new_gallery_page.data.length; i++) {
		$gallery_link = $("<a />", {"target":"_blank", "href":"/gallery?id=" + new_gallery_page.data[i].id});
		$curr_pic = $("<img />", {"class":"image", "src":new_gallery_page.data[i].photos.data[0].images[0].source});
		$img_container = $("<div />", {"class":"container"});
		$middle = $("<div />", {"class":"middle"});

		$gallery_link.html("<span class = text>" + new_gallery_page.data[i].name);
		$($middle).append($gallery_link);
		$($img_container).append($curr_pic);
		$($img_container).append($middle);
		$($gallery_div).append($img_container);
	}
}

var qs = {
	qsArray : [],
	qsHash : {},
	init : function() {	
		if (window.location.search.length > 1) {
			qs.qsArray = window.location.search.substr(1).split("&");
			
			for (let i = 0; i < qs.qsArray.length; i++) 
			{ 
				let item = qs.qsArray[i].split("=");
				qs.qsHash[item[0]] = item[1];
			}
		}
	},
	get : function(id) {
		return qs.qsHash[id];
	}
}
qs.init();
