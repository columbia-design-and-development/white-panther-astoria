import React from 'react'
import Header from './Header'
import Main from './Main'
import './../css/App.css';

const App = () => (
  <div>
    <Header />
    <Main />
  </div>
)

export default App
