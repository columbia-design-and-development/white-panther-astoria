import React from 'react'
import { NavLink } from 'react-router-dom'

import './../css/Header.css';

const Header = () => (
  <header>
    <nav>
      <ul>
        <li><NavLink to="/" activeClassName="link-active" exact={true}>Home</NavLink></li>
        <li><NavLink to="/about-us" activeClassName="link-active">About Us</NavLink></li>
        <li><NavLink to="/gallery" activeClassName="link-active">Gallery</NavLink></li>
      </ul>
    </nav>
  </header>
)

export default Header
